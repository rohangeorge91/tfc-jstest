#STEPS

1. Prepare the shell-script using 'curl' to call the various API's.
2. Start the python-bottle server.
3. Run the bash script and find the throughput of each API under extreme overloading.
4. We can use the the test script located at /tfc-client/test/shellScriptRestfulTest/shellScriptTest.sh
