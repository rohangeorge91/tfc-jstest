#/bin/bash
user_set_multipler=10; #10 threads
test_per_api=2000;

function_jstest(){
  i=0
  T1="$(date +%s)"
  while [ $i -lt $test_per_api ]
    do
      output=$(curl -s http://localhost:8000/jstest)
      i=$(expr $i + 1)
    done
  T2="$(($(date +%s)-T1))"
  avg=$(bc <<<"scale=4; $T2/$test_per_api");
  printf "\njstest test\n http://localhost:8000/api/hash \n Request servered : %s \n Total time : %s Seconds\n Average time per request : %s Seconds\n" "$test_per_api" "$T2" "$avg";
}

function_hash(){
  i=0
  T1="$(date +%s)"
  while [ $i -lt $test_per_api ]
    do
      word=$(tr -cd A-Za-z0-9 </dev/urandom | head -c 10)
      output=$(curl -H "Content-Type: application/json" -X POST -d "{\"input\" : \"$word\"}" -s http://localhost:8000/api/hash)
      i=$(expr $i + 1)
    done
  T2="$(($(date +%s)-T1))"
  avg=$(bc <<<"scale=4; $T2/$test_per_api");
  printf "\nHash API\n http://localhost:8000/api/hash \n Request servered : %s \n Total time : %s Seconds\n Average time per request : %s Seconds\n" "$test_per_api" "$T2" "$avg";
}

function_counter(){
  i=0
  T1="$(date +%s)"
  while [ $i -lt $test_per_api ]
    do
      word=$(tr -cd 0-9 </dev/urandom | head -c 5)
      output=$(curl -H "Content-Type: application/json" -X POST -d "{\"input\" : \"$word\"}" -s http://localhost:8000/api/counter)
      i=$(expr $i + 1)
    done
  T2="$(($(date +%s)-T1))"
  avg=$(bc <<<"scale=4; $T2/$test_per_api");
  printf "\nCounter API\n http://localhost:8000/api/hash \n Request servered : %s \n Total time : %s Seconds\n Average time per request : %s Seconds\n" "$test_per_api" "$T2" "$avg";
}

function_global_counter(){
  i=0
  T1="$(date +%s)"
  while [ $i -lt $test_per_api ]
    do
      word=$(tr -cd 0-9 </dev/urandom | head -c 5)
      output=$(curl -H "Content-Type: application/json" -X POST -d "{\"input\" : \"$word\"}" -s http://localhost:8000/api/global-counter)
      i=$(expr $i + 1)
    done
  T2="$(($(date +%s)-T1))"
  avg=$(bc <<<"scale=4; $T2/$test_per_api");
  printf "\nGlobal counter API\n http://localhost:8000/api/hash \n Request servered : %s \n Total time : %s Seconds\n Average time per request : %s Seconds\n" "$test_per_api" "$T2" "$avg";
}

count=0;
printf "\nAPI Break-point test \n User count per API (Parallel calls to server) : %s \n Total number of server call per API : %s\n Total number of API's : %s\n\n" "$user_set_multipler" "$test_per_api" "4"

while [ $count -lt $user_set_multipler ]
do
  function_jstest &
  function_hash &
  function_counter &
  function_global_counter &
  count=$(expr $count + 1)
done
wait
