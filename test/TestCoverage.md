#server-side

Most of the test involves generating random data and sending it to the API.
1. Alphabets of the numerical API's (Try & catch placed for non-numberical and empty inputs)
2. Try sending random 10 letter words with A-Za-z0-9 characters. (special characters are encoded by the browser and direct use don't work with curl).

#client-side
1. API test via the application, using browser inputs.
2. Testing the UI Threshold using large data-set for the output (10^5). Fixed a bug with large data-set messing with angularJS using a Lazy-Load with
the help of filters in the angularJS API.
3. Responsive UI check using various screen resolution, CSS functionality and its issues.
4. Parallel user browser test. (found issue with duplicate entries for the same data in the output, fixed)

Selenium - Maybe a better option for automated testing.
SoapUI can help with the restful UI testing.
