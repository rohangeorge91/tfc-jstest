# The main stuff
FROM ubuntu:14.04
MAINTAINER Verghese Koshy <vkputhukkeril@gmail.com>
RUN apt-get update

#Application stuff
RUN apt-get --assume-yes install python3
RUN apt-get --assume-yes install python3-setuptools
RUN easy_install3 pip

#Install the server and the necessary libraries
RUN pip3 install bottle
RUN pip3 install CherryPy

#clone the repo and place at root directory -> NOTE: Please change the directory to the required path.
RUN mkdir tfc-jstest
COPY . /tfc-jstest/

#start point
ENTRYPOINT ["/bin/bash", "tfc-jstest/server/runserver.sh", "0.0.0.0", "8000"]
