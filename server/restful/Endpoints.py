"""The Module is resposible for all restful endpoints for the given server"""
import sys
import hashlib
import threading
import datetime

from bottle import Bottle
from bottle import request
from bottle import static_file

#if sys.argv is not null then try to set it.
ARGUMENT_1 = sys.argv[1] if sys.argv[1] is not None else 'localhost'
ARGUMENT_2 = int(sys.argv[2]) if sys.argv[2] is not None else 8000
#Bug fix 2 : Added ../ to help with starting the python program in different directory
ARGUMENT_3 = sys.argv[3] + '../client/resource' if sys.argv[3] is not None or \
sys.argv[3] is '' else '../client/resource'

class App(object):
    '''The Webapp class'''
    # pylint: disable=too-many-instance-attributes
    def __init__(self, host, port, static_file_root):
        '''Initialize the class'''
        self._host = host
        self._port = port
        self._static_file_root = static_file_root

        #A global count which is used by all the users.
        self.global_count = 0
        self.request_count = 0

        #List of data
        self.data_list = []
        self.last_update = datetime.datetime.now()
        self.thread_cond = threading.Condition()

        self._app = Bottle()
        self.route_mapper()

    def start(self):
        '''Start the server.'''
        self._app.run(server="cherrypy", host=self._host, port=self._port, debug=True)

    #The function routing of restful API & the client files
    def route_mapper(self):
        '''Default Home directory handler.'''
        self._app.route('/', method=["GET", "POST"], callback=self.home_directory_handler)
        self._app.route('/jstest', method=["GET", "POST"], callback=self.home_directory_handler)
        self._app.route('/client/<filename:path>', method=["GET", "POST"], \
        callback=self.static_file_handler)

        self._app.route('/api/global-counter', method='POST', callback=self.global_counter_api)
        self._app.route('/api/hash', method='POST', callback=self.md5_hash_api)
        self._app.route('/api/counter', method='POST', callback=self.counter_api)
        self._app.route('/api/display-all', method="POST", callback=self.display_all)

    #The function sets the home-directory 'or' root HTML page
    def home_directory_handler(self):
        """The function returns the root homepage for the HTML content"""
        return static_file("index.html", root=self._static_file_root)

    #The function will return a number to the user which is global static.
    def global_counter_api(self):
        """Returns a Global-Counter for a given user."""
        self.increment_request()

        try:
            lock = threading.Lock()
            data = request.json.get("input")
            data_int = int(data)

            #value is lesser than 0 then we return falsed
            if data_int < 0:
                json_data = {"input" : data, "type" : ["error"], "error" : \
                "Input value is lesser than 0", "success" : False, \
                "operation" : "global count", "datetime" : str(datetime.datetime.now())}
                return self.add_message(json_data)

            #Valid increment value was supplied
            try:
                lock.acquire()
                old_global_count = self.global_count
                self.global_count = self.global_count + data_int

                json_data = {"input" : data, "type" : ["old counter", "output"], \
                "output" : self.global_count, "old counter" : old_global_count, \
                "success" : True, "operation" : "global count", \
                "datetime" : str(datetime.datetime.now()), "request_count" : self.request_count}
                return self.add_message(json_data)

            #Runtime issue with the lock.
            except RuntimeError as err:
                print(str(err))

                json_data = {"input" : data, "type" : ["error"], "error" : \
                "There is a issue at the server-side!", "success" : False, \
                "operation" : "global count", "datetime" : str(datetime.datetime.now()), \
                "request_count" : self.request_count}
                return self.add_message(json_data)

            finally:
                lock.release()

        except ValueError:

            json_data = {"input" : data, "type" : ["error"], "error" : \
            "The value is not a number", "success" : False, "datetime" : \
            str(datetime.datetime.now()), "operation" : "global count", \
            "request_count" : self.request_count}
            return self.add_message(json_data)

    #Returns the md5 hashvalue of the string which is passed to the 'data' element
    #from the given JSON object.
    def md5_hash_api(self):
        """Returns a MD5 hash value for the given string"""
        self.increment_request()

        data = request.json.get("input")

        if data is None:
            json_data = {"input" : "", "type" : ["error"], "error" : \
            "input variable is not defined!", "datetime" : str(datetime.datetime.now()), \
            "success" : False, "operation" : "hash", "request_count" : self.request_count}
            return self.add_message(json_data)

        data_utf16 = data.encode("UTF-8")
        md5_data = hashlib.md5(data_utf16).hexdigest()
        json_data = {"input" : data, "type" : ["md5"], "md5" : md5_data, \
        "success" : True, "datetime" : str(datetime.datetime.now()), "operation" : "hash", \
        "request_count" : self.request_count}
        return self.add_message(json_data)

    #Returns the incremented value of the user data.
    def counter_api(self):
        """Returns an incremented value of the count"""
        self.increment_request()
        data = request.json.get("input")

        try:
            data_int = int(data)

            if data_int < 0:
                json_data = {"input" : data, "type" : ["error"], "error" : \
                "User value less than zero", "success" : False, "datetime" : \
                str(datetime.datetime.now()), "operation" : "hash", "request_count" : \
                self.request_count}
                return self.add_message(json_data)

            data_int += 1

            json_data = {"input" : data, "type" : ["output"], "output" : data_int, \
            "success" : True, "operation" : "counter", "datetime" : str(datetime.datetime.now()), \
            "request_count" : self.request_count}
            return self.add_message(json_data)

        except ValueError:
            json_data = {"input" : data, "type" : ["error"], "error" : \
            "User data is not a number", "success" : False, "operation" : \
            "counter", "datetime" : str(datetime.datetime.now()), "request_count" : \
            self.request_count}
            return self.add_message(json_data)

    #Display all the data in the system
    def display_all(self):
        '''Display all the data stored in the system'''

        if request.json is None:
            return {"lastUpdate" : str(self.last_update), "data" : self.data_list}

        date = request.json.get("lastUpdate")

        if date is None:
            return {"lastUpdate" : str(self.last_update), "data" : self.data_list}

        #wait-here
        if str(self.last_update) <= str(date):
            threading.currentThread()
            with self.thread_cond:
                self.thread_cond.wait((60.0 * 30))

        if str(self.last_update) <= str(date):
            return {"success" : "true", "type" : "error", "error" : \
            "no update from last time-stamp"}

        return {"lastUpdate" : str(self.last_update), "data" : self.update_list(date)}

    #Returns an list with only the change since after the given date
    def update_list(self, date):
        '''Returns back the list which has updates after the given time interval'''
        update_entry = []
        for member in self.data_list:
            if str(member["datetime"]) <= str(date):
                break

            update_entry.insert(0, member)
        return update_entry

    #Default Static content handler
    def static_file_handler(self, filename):
        """The function provides a default file handler for static web content."""
        return static_file(filename, root=self._static_file_root)


    #increment the request counter by 1
    def increment_request(self):
        '''The function basically increments the request_count of the class'''
        self.request_count = self.request_count + 1

    #added the data into the list.
    def add_message(self, data):
        '''The function basically add the message to the list.'''
        self.data_list.insert(0, data)
        self.last_update = data["datetime"]

        threading.currentThread()
        with self.thread_cond:
            self.thread_cond.notifyAll()

        return data

#Start the python bottle class.
WEBAPP = App(host=ARGUMENT_1, port=ARGUMENT_2, static_file_root=ARGUMENT_3)
WEBAPP.start()
