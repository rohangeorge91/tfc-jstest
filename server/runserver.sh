#!/usr/bin/env bash

#Default variable if values not passed.
_SHELL=$0;
_IP_ADDR=${1:-localhost};
_PORT=${2:-8000};

#Start the server with the set IP_ADDR and port
startServer(){
  #Bug fix 1: Allows the executing of the bash script from any directory
  root="${_SHELL/runserver.sh/}"
  echo "Executing Python-3 Server..."
  output=$(python3 ${root}restful/Endpoints.py $_IP_ADDR $_PORT "${root}")

}

#Function check if arguments are available, else supply default.
getArguments(){

  #if two or zero arguments are passed then if default behaviour
  if [ "$#" == 2 ];
  then
    echo "Setting IP_ADDR as $_IP_ADDR and PORT as $_PORT ..."
    startServer

  elif [ "$#" == 0 ];
  then
    echo "Using default IP_ADDR $_IP_ADDR and PORT $_PORT ..."
    startServer

  else
    echo "Usage sh runserver.sh [IP_ADDR] [PORT]"
    echo "IP_ADDR specify the IP Address or the URL endpoint"
    echo "PORT specify the port of the server"

  fi
}

#Start the server using the arguments.
getArguments
