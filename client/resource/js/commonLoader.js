//Dependencies CSS
require('!style!css!../bower_components/bootstrap/dist/css/bootstrap.min.css');
require('!style!css!../bower_components/bootstrap/dist/css/bootstrap-theme.min.css');
require('!style!css!../bower_components/font-awesome/css/font-awesome.min.css');

//Custom css
require('!style!css!../css/main.css');

//Dependencies JS Libraries
window.jQuery = window.$ = require('../bower_components/jquery/dist/jquery.min.js');
require('../bower_components/bootstrap/dist/js/bootstrap.min.js');
require('../bower_components/angular/angular.min.js');
require('../bower_components/angular-resource/angular-resource.min.js');
require('../bower_components/angular-animate/angular-animate.min.js');

//Custom Application JS
require('../js/client.js');
