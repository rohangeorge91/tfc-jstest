var tfcApp = angular.module('tfc_JSTest', ['ngAnimate']);

//ignoreSingleLine
tfcApp.config([
  '$httpProvider',
  function($httpProvider) {
    $httpProvider.defaults.timeout = (60 * 30);
  },
]);

$(window).scroll(function() {
  if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    $('#scrollUpdate').click();
  }
});

tfcApp.controller('viewerCtrl', function($scope, $rootScope, $timeout, $http) {

  $scope.displayLimit = 20;
  $rootScope.messages = [];

  $scope.scrollUpdate = function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $scope.displayLimit += 10;
    }//$rootScope.messages.unshift
  };

  $scope.updateDisplay = function() {
    messages = $rootScope.messages;
    lastUpdate = $rootScope.lastUpdate;

    if (lastUpdate === null) {

      $http.post('/api/display-all').success(function(data) {
        $rootScope.messages  = data.data;
        $rootScope.lastUpdate = data.lastUpdate;

        $timeout($scope.updateDisplay, 1001);
      }).error(function(data) {
        $timeout($scope.updateDisplay, 1000);
      });

    }else {
      var jsonData = {};
      jsonData.lastUpdate = lastUpdate;

      $http.post('/api/display-all', jsonData).success(function(data) {
        //Bug fix 3: The js check modified to avoid accidental copy of the data entry.
        if (Date.parse(lastUpdate) < Date.parse(data.lastUpdate)) {
          $rootScope.lastUpdate = data.lastUpdate;
          dataSet = data.data;
          for (i = 0; i < dataSet.length; i++) {
            $rootScope.messages.unshift(dataSet[i]);
          }
        }

        $timeout($scope.updateDisplay, 1000);
      }).error(function(data) {
        $timeout($scope.updateDisplay, 1000);
      });
    }
  };

  $http.post('/api/display-all').success(function(data) {
    $rootScope.messages  = data.data;
    $rootScope.lastUpdate = data.lastUpdate;

    $timeout($scope.updateDisplay, 1000);
  });

});

tfcApp.controller('interfaceCtrl', function($scope, $rootScope, $http, $element) {
  $scope.sendData = function(postUrl) {
    var inputVal = $element.parentsUntil('#input-data-form').find('#value-box').val();
    var jsonData = {};
    jsonData.input = inputVal;

    $http.post(postUrl, jsonData).success(function(data) {
      //$rootScope.messages.unshift(data);
      //$rootScope.lastUpdate = data.datetime;
      //console.log(data)
    });
  };

  $scope.hash = function() {
    var postUrl = '/api/hash';
    $scope.sendData(postUrl);
  };

  $scope.counter = function() {
    var postUrl = '/api/counter';
    $scope.sendData(postUrl);
  };

  $scope.globalCounter = function() {
    var postUrl = '/api/global-counter';
    $scope.sendData(postUrl);
  };
});
