module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //Minify the code from production ready js
    uglify: {
      options: {
        banner: '/*! Uglify : <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        mangle: false,
        sourceMap: false,
      },
      build: {
        src: 'resource/js/prod/<%= pkg.name %>.js',
        dest: 'resource/js/prod/<%= pkg.name %>.min.js',
      },
    },

    //The watch which keeps strack of change
    watch: {
      files: ['resource/js/prod/tfc-client-app.js'],
      tasks: ['uglify'],
    },

    //Webpack builder
    webpack: {
      buildup: {
        entry: './resource/js/commonLoader.js',
        output: {
          path: 'resource/js/prod/',
          filename: 'tfc-client-app.js',
        },
        module: {
          loaders: [
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&minetype=application/font-woff' },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' },
          ],
        },
        stats: {
          // Configure the console output
          colors: false,
          modules: true,
          reasons: true,
        },

        //Watch for chane in source files
        watch: true,
      },
    },
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Load the plugin that provides the "webpack" task
  grunt.loadNpmTasks('grunt-webpack');

  //Watcher
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['webpack', 'uglify', 'watch']);

};
